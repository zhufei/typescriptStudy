mkdir zerots
cd zerots
npm init -y
npm install typescript --save-dev
npm install @types/node --save-dev
node ./node_modules/typescript/lib/tsc --init

npm install ts-node --save-dev
npm install nodemon --save-dev

modify package.json

"scripts": {
  "start": "npm run build:live",
  "build:live": "nodemon --exec ./node_modules/.bin/ts-node -- ./index.ts"
},


echo 'console.log("hello world");' > index.ts
npm start





https://blog.programster.org/getting-started-with-node-typescript

Getting Started with Node + Typescript
NodeJS TypeScript
The steps below will get you started with creating a new NodeJS application that you program using Typescript. The content is mostly straight out of this gitbook if you prefer to read from the original source.

Steps
Create and use an area for a new project

mkdir my-project
cd my-project
Copy
Initialize a the project with a package.json file

npm init -y
Copy
Now setup for typescript

npm install typescript --save-dev
npm install @types/node --save-dev
node ./node_modules/typescript/lib/tsc --init
Copy
Run these commands to have the typescript files compile into javascript and then have node application restart whenever a file is saved and compiled.

npm install ts-node --save-dev
npm install nodemon --save-dev
Copy
Now edit your scripts attribute in your package.json file to automatically build and run the index.ts file whenever we make a change to it.

"scripts": {
  "start": "npm run build:live",
  "build:live": "nodemon --exec ./node_modules/.bin/ts-node -- ./index.ts"
}
Now you can get started building your node application. The example below will create a process that will print out "hello world" when the process is executed.

echo 'console.log("hello world");' > index.ts
npm start
Copy
NodeJS lets you run standalone javascript applications/processes. If you want it to act as a webserver, you have to write the application to listen to the appropriate port and handle the html protocol. Luckily most of the work is done for you.
